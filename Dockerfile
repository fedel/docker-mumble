FROM debian

RUN apt-get update
RUN apt-get install -y git mumble-server nodejs npm
RUN apt-get install -y websockify
# add user node
RUN groupadd -g 1001 node
RUN useradd -g 1001 -u 1001 -m node && \  
    mkdir -p /home/node/.npm-global && \                
    mkdir -p /home/node/app 

USER node

WORKDIR /home/node
RUN git clone --branch=webrtc https://github.com/Johni0702/mumble-web/
RUN cd /home/node/mumble-web && \
    npm install && \
    npm run build 

USER root

EXPOSE 80

CMD service mumble-server start && websockify --ssl-target --web=/home/node/mumble-web/dist 80 127.0.0.1:64738

